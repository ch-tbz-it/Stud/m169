# M169 - 20 Infrastruktur

Durch die Nutzung von Infrastructure as Code und Cloud-Services können Sie Ihre IT-Infrastruktur effizient automatisieren, was Ihnen Zeit spart, Skalierbarkeit ermöglicht und für konsistente Umgebungen in der Softwareentwicklung und Systemverwaltung sorgt.

#### Inhaltsverzeichnis

* 01 - [Cloud Computing](#01---cloud-computing)
* 02 - [Infrastructure as Code](#02---infrastructure-as-code)
* 03 - [AWS Cloud (optional)](#03---aws-cloud)
* 04 - [Azure Cloud (optional)](#04---azure-cloud)
* 05 - [Wissenswert](#05---wissenswert)
* 06 - [Mögliche Serverdienste für die Automatisierung](https://wiki.ubuntuusers.de/Serverdienste/)
___

![](../images/Cloud_Computing_36x36.png "Cloud Computing") 01 - Cloud Computing
======


> [⇧ **Nach oben**](#inhaltsverzeichnis)

[![](https://img.youtube.com/vi/36zducUX16w/0.jpg)](https://www.youtube.com/watch?v=36zducUX16w)

Cloud Computing Services Models - IaaS PaaS SaaS Explained

---

Unter **Cloud Computing** (Rechnerwolke) versteht man die Ausführung von Programmen, die nicht auf dem lokalen Rechner installiert sind, sondern auf einem anderen Rechner, der aus der Ferne (remote) aufgerufen wird.

Technischer formuliert umschreibt das Cloud Computing den Ansatz, IT-Infrastrukturen (z.B. Rechenkapazität, Datenspeicher, Datensicherheit, Netzkapazitäten oder auch fertige Software) über ein Netz zur Verfügung zu stellen, ohne dass diese auf dem lokalen Rechner installiert sein müssen.

Angebot und Nutzung dieser Dienstleistungen erfolgen dabei ausschliesslich über technische Schnittstellen und Protokolle sowie über Browser. Die Spannweite der im Rahmen des Cloud Computings angebotenen Dienstleistungen umfasst das gesamte Spektrum der Informationstechnik und beinhaltet unter anderem Infrastruktur, Plattformen und Software (IaaS, PaaS und SaaS).


### Arten von Cloud Computing
***

**Infrastruktur – Infrastructure as a Service (IaaS)** <br>
Die Infrastruktur (auch "Cloud Foundation") stellt die unterste Schicht im Cloud Computing dar. Der Benutzer greift hier auf bestehende Dienste innerhalb des Systems zu, verwaltet seine Recheninstanzen (virtuelle Maschinen) allerdings weitestgehend selbst. 

**Plattform – Platform as a Service (PaaS)** <br>
Der Entwickler erstellt die Anwendung und lädt diese in die Cloud. Diese kümmert sich dann selbst um die Aufteilung auf die eigentlichen Verarbeitungseinheiten. Im Unterschied zu IaaS hat der Benutzer hier keinen direkten Zugriff auf die Recheninstanzen. Er betreibt auch keine virtuellen Server.

**Anwendung – Software as a Service (SaaS)** <br>
Die Anwendungssicht stellt die abstrakteste Sicht auf Cloud-Dienste dar. Hierbei bringt der Benutzer seine Applikation weder in die Cloud ein, noch muss er sich um Skalierbarkeit oder Datenhaltung kümmern. Er nutzt eine bestehende Applikation, die ihm die Cloud nach aussen hin anbietet.

Mit dem Advent von Docker (Containierisierung) hat sich zwischen IaaS und PaaS eine neue Ebene geschoben: 

**CaaS (Container as a Service)** <br>
Diese Ebene ist dafür zuständig, containerisierten Workload auf den Ressourcen auszuführen, die eine IaaS-Cloud zur Verfügung stellt. Die Technologien dieser Ebene wie Docker, Kubernetes oder Mesos sind allesamt quelloffen verfügbar. Somit kann man sich seine private Cloud ohne Gefahr eines Vendor Lock-ins aufbauen.


### Dynamic Infrastructure Platforms
***

[![](https://img.youtube.com/vi/KXkBZCe699A/0.jpg)](https://www.youtube.com/watch?v=KXkBZCe699A)

Microsoft How does Microsoft Azure work

---

Eine dynamische Infrastruktur-Plattform (Dynamic Infrastructure Platform) ist ein **System**, das Rechen-Ressourcen  virtualisiert und bereitstellt, insbesondere CPU (**compute**), Speicher (**storage**) und Netzwerke (**networks**). 
Die Ressourcen werden programmgesteuert zugewiesen und verwaltet. Die Bereitstellung erfolgt mit virtuellen Maschinen (VM).

Beispiele sind:
*  Public Cloud
    *  *AWS, Azure, Digital Ocean, Google, exoscale*
*  Private Cloud 
    *  *CloudStack, OpenStack, VMware vCloud*
*  Lokale Virtualisierung 
    *  *Oracle VirtualBox, Hyper-V, VMware Player*
*  Hyperkonvergente Systeme 
    *  *Rechner die die oben beschriebenen Eigenschaften in einer Hardware vereinen*

**Anforderungen**
Damit Infrastructure as Code (IaC) auf dynamischen Infrastruktur-Plattformen genutzt werden kann, müssen sie die folgenden Anforderungen erfüllen:
* **Programmierbar**
    * Ein Userinterface ist zwar angenehm und viele Cloud Anbieter haben ein solches, aber für IaC muss die Plattform via Programmierschnittstelle (API) ansprechbar sein.
* **On-demand**
    * Ressourcen (Server, Speicher, Netzwerke) schnell erstellen und vernichtet.
* **Self-Service**
    * Ressourcen anpassen und auf eigene Bedürfnisse zuschneiden.
* **Portabel**
    * Anbieter von Ressourcen (z.B. AWS, Azure) müssen austauschbar sein.
* Sicherheit, Zertifizierungen (z.B. ISO 27001), etc.



![](../images/Software-Konfiguration_36x36.png "Infrastructure as Code") 02 - Infrastructure as Code
====

> [⇧ **Nach oben**](#inhaltsverzeichnis)

**Früher** <br>
In der "Eisenzeit" der IT, waren die IT-Systeme physikalisch an Hardware gebunden. Die Bereitstellung und Aufrechterhaltung der Infrastruktur war manuelle Arbeit. Dabei wurde viel Arbeitszeit investiert, die Systeme bereitzustellen und am Laufen zu halten. Änderungen waren hingegen teuer aufwendig.

**Heute** <br>
Im heutigen "Cloud-Zeitalter" der IT, sind Systeme von der physikalischen Hardware entkoppelt, sie sind Virtualisiert.
Bereitstellung und Wartung können an Software-Systeme delegiert werden und befreien die Menschen von Routinearbeiten.
Änderungen können in Minuten, wenn nicht sogar in Sekunden vorgenommen werden. Das Management kann diese Geschwindigkeit, für einen schnelleren Marktzugang ausnutzen.


### Definition
***
Infrastructure as Code (IaC) ist ein **Paradigma** (grundsätzliche Denkweise) zur Infrastruktur-Automation basierend auf Best Practices der Softwareentwicklung.

Im Vordergrund von IaC stehen konsistente und wiederholbare Definitionen für die Bereitstellung von Systemen und deren Konfiguration. Die Definitionen werden in Dateien zusammengefasst, gründlich Überprüft und automatisch ausgerollt.

Dabei kommen, von der Softwareentwicklung bekannte, Best Practices zum Einsatz:
* Versionsverwaltung - Version Control Systems (VCS)
* Testgetriebene Entwicklung - Testdriven Development (TDD)
* Kontinuierliche Integration - Continuous Integration (CI)
* Kontinuierliche Verteilung - Continuous Delivery (CD)


### Ziele
***
Ziele von **Infrastructure as a Code** (IaC) sind:
* IT-Infrastruktur wird unterstützt und ermöglicht Veränderung, anstatt Hindernis oder Einschränkung zu sein.
* Änderungen am System sind Routine, ohne Drama oder Stress für Benutzer oder IT-Personal.
* IT-Mitarbeiter verbringen ihre Zeit für wertvolle Dinge, die ihre Fähigkeiten fördern und nicht für sich wiederholende Aufgaben.
* Fachanwender erstellen und verwalten ihre IT-Ressourcen, die sie benötigen, ohne IT-Mitarbeiter
* Teams sind in der Lage, einfach und schnell, ein abgestürztes System wiederherzustellen.
* Verbesserungen sind kontinuierlich und keine teuren und riskanten "Big Bang" Projekte.
* Lösungen für Probleme sind durch Implementierung, Tests, und Messen institutionalisiert, statt diese in Sitzungen und Dokumente zu erörtern.


### Tools
***
Folgende Arten von Tools werden für IaC benötigt:
* **Infrastructure Definition Tools**
    * Zur Bereitstellung und Konfiguration einer Sammlung von Ressourcen (z.B. OpenStack, TerraForm, CloudFormation)
* **Server Configuration Tools**
    * Zur Bereitstellung und Konfiguration von Servern bzw. VMs (z.B. Vagrant, Packer, Docker)
* **Package Management Tools**
    * Zur Bereitstellung und Verteilung von vorkonfigurierter Software, vergleichbar mit einem APP-Store. Bei Linux: APT, YUM, bei Windows: WiX, Plattformneutral: SBT native packager
* **Scripting Tools**
    * Kommandozeileninterpreter, kurz CLI (Command-Line Interpreter / Command-Line Shell), zur Schrittweisen Abarbeitung von Befehlen. Bei Linux, Mac und Windows 10: Bash, bei reinem Windows: PowerShell.
* **Versionsverwaltung & Hubs**
    * Zur Versionskontrolle der Definitionsdateien und als Ablage vorbereiteter Images. (z.B. GitHub, Vagrant Boxes, Docker Hub, Windows VM)

### Vergleich der Lösungsansätze
**Vagrant**
Vagrant ist ein Tool, das zum Erstellen und Verwalten von virtuellen Maschinenumgebungen in Single-Host-Infrastrukturen verwendet wird. Es basiert auf einem einfachen Workflow und ermöglicht es Benutzern, Entwicklungs- und Testumgebungen schnell und konsistent zu konfigurieren. Obwohl Vagrant primär für die Lokalisierung von Entwicklungsumgebungen verwendet wird, unterstützen viele Cloudanbieter die Integration von Vagrant, um Cloud-Instanzen auf ähnliche Weise wie lokale virtuelle Maschinen zu verwalten.

Cloudanbieter, die Vagrant unterstützen oder mit denen Vagrant durch Plugins oder Drittanbieter-Tools integriert werden kann, umfassen:

* AWS (Amazon Web Services): Durch das offizielle Vagrant AWS-Plugin können Entwickler AWS-Instanzen direkt mit Vagrant verwalten.

* Microsoft Azure: Ein Vagrant-Plugin für Azure ermöglicht es, Vagrant-Umgebungen in Azure-VMs zu deployen und zu verwalten.

* Google Cloud Platform (GCP): Es gibt ein Vagrant-Plugin für GCP, das die Nutzung von Google Compute Engine-Instanzen mit Vagrant ermöglicht.

* VMware: VMware bietet Unterstützung für Vagrant durch ein spezifisches Plugin, das die Verwendung von VMware-basierten virtuellen Maschinen mit Vagrant ermöglicht.

* DigitalOcean: Mit dem Vagrant DigitalOcean-Plugin können Benutzer DigitalOcean-Droplets als Vagrant-Umgebungen erstellen und verwalten.

* Oracle Cloud Infrastructure (OCI): Es gibt Plugins und Tools, die die Integration von Vagrant mit OCI ermöglichen, um VMs in der Oracle Cloud zu verwalten.

Für die Nutzung von Vagrant mit diesen Cloudanbietern ist es notwendig, das entsprechende Plugin oder Tool zu installieren und zu konfigurieren. Die meisten dieser Plugins sind Open Source und auf Plattformen wie GitHub/GitLab verfügbar, wo sie von der Community gepflegt werden.

Vagrant fokussiert auf die Bereitstellung und Verwaltung von virtuellen Maschinen (VMs). Es nutzt Hypervisor-Technologien wie VirtualBox, VMware oder Hyper-V, um komplette Gastbetriebssysteme zu virtualisieren. Vagrant wird typischerweise für die Erstellung einheitlicher Entwicklungsumgebungen verwendet, indem es VMs gemäss einer vordefinierten Konfiguration (Vagrantfile) automatisiert aufsetzt. Dadurch können Systeme anhand der Konfigurationsdatei, genannt Vagrantfile", mehrfach in unterschiedlichen Umgebungen effizient reproduziert werden. Allerdings wird dabei immer der gesamte Betriebssystem-Stack dupliziert, was mehr Ressourcen verbraucht als das bei einem containerbasiertem System der Fall ist.

Vagrant Tutorials und mehr Informationen siehe [hashicorp - Vagrant](https://developer.hashicorp.com/vagrant)

Wenn auch Vagrant nicht in jedem Fall die richtige Wahl ist, bleibt es ein wichtiges Tool in der Entwicklung und im Betrieb, insbesondere für das Aufsetzen und Verwalten von virtuellen Maschinen (VMs) in Entwicklungsumgebungen. Es hat sich seit seiner Einführung im Jahr 2010 eine solide Nutzerbasis aufgebaut und wird für verschiedene Anwendungsfälle, insbesondere in der Softwareentwicklung und beim Testen von Anwendungen, geschätzt. Die Stärken von Vagrant liegen in seiner Einfachheit, Portabilität und der Möglichkeit, konsistente Entwicklungsumgebungen zu schaffen, die das "Works on my machine" Problem adressieren (also ohne Einbindung in ein Gesamtsystem / Cloudumgebung).

Die Relevanz von Vagrant hängt stark von den spezifischen Bedürfnissen eines Projekts oder einer Organisation ab. Für Szenarien, in denen vollständige virtuelle Maschinen benötigt werden, bietet Vagrant weiterhin erhebliche Vorteile. Es ist besonders nützlich in Situationen, wo:

* Entwicklungsumgebungen reproduziert werden müssen, um die Konsistenz zwischen verschiedenen Teammitgliedern oder CI/CD-Pipelines zu gewährleisten.
* Mehrere VM-basierte Umgebungen gleichzeitig verwaltet werden müssen, insbesondere wenn diese Umgebungen komplexe Netzwerkkonfigurationen oder mehrere Dienste umfassen.
* Projekte auf bestimmte Betriebssysteme oder spezifische Betriebssystemkonfigurationen angewiesen sind, die in Containern schwer zu replizieren sind.

Jedoch, mit dem Aufkommen und der zunehmenden Popularität von Containerisierungstechnologien wie Docker und Container-Orchestrierungstools wie Kubernetes, hat sich der Fokus vieler Entwicklungs- und Betriebsteams verschoben. Diese Tools bieten eine leichtgewichtigere, effizientere Alternative zu VMs für viele Anwendungsfälle, insbesondere in Produktionsumgebungen und bei der Entwicklung von Mikroservices.

Trotzdem bleibt Vagrant relevant für bestimmte Use Cases und Nutzergruppen, die VMs bevorzugen oder benötigen. Es wird aktiv entwickelt und unterstützt und hat eine aktive Community. Die Entscheidung, ob Vagrant oder eine andere Technologie wie Docker für ein Projekt am besten geeignet ist, hängt von den spezifischen technischen Anforderungen und Vorlieben ab.

### Tabellarischer Vergleich der unterschiedlichen Tools:
| Anbieter               | Produkte                        | Verwendungszweck                           | Technische Basis           |
|------------------------|---------------------------------|--------------------------------------------|----------------------------|
| Proxmox Solutions GmbH | Proxmox VE                      | Server-, Storage- und Netzwerkvirtualisierung, HCI | Linux, KVM/QEMU, ZFS, Ceph |
| Microsoft              | Windows Server 2022 mit Hyper-V, Azure Stack HCI | Server-, Storage- und Netzwerkvirtualisierung, HCI | Hyper-V                    |
| Red Hat                | OpenShift Virtualization        | Integration von VMs in Containerumgebungen | Linux, KVM/QEMU, Kubernetes|
| Oracle                 | Oracle Virtualization Manager   | Servervirtualisierung                      | Linux, KVM/QEMU, oVirt     |
| SUSE                   | Harvester                       | Cloud-native HCI, Integration in Containerumgebungen | Linux, KVM/QEMU, Kubernetes, Ranger |
| Nutanix                | Produkte der Nutanix-Cloud-Plattform | Server- und Storage-Virtualisierung, HCI, hybride Infrastruktur, Cloud-Integration | AHV (Acropolis Hypervisor), Acropolis OS |
| HashiCorp              | Vagrant                         | Automatisierung von **Entwicklungsumgebungen** | Ruby, VirtualBox, VMware, Docker, etc. (abhängig vom Provider) |


<br>

![](../images/AWS_36x36.png "AWS Cloud") 03 - AWS Cloud
======

> [⇧ **Nach oben**](#inhaltsverzeichnis)

### Grundlagen
***

**Root Account** <br>
Bezeichnet den Inhaber des AWS-Benutzerkontos. Für den Root sind alle Funktionen in der Cloud freigeschaltet, weshalb mit diesem Benutzer nicht direkt gearbeitet werden soll.

**Regionen** <br>
AWS hat unabhängige Rechenzentren in unterschiedlichen Regionen der Welt, z.B. Irland, Frankfurt, Virginia

**IAM User** <br>
Identity-Management (IAM) ist ein Verwaltungssystem, welches dem Root erlaubt, eigenständige User anzulegen und mit unterschiedlichen Rechten (Permissions & Policies) auszustatten. 

**Network and Security** <br>
Bei AWS gibt es eine Funktion in der EC2-Konsole, welche es erlaubt Security Groups, Key Pairs etc. zu verwalten.

*Security Groups* legen fest welche Ports nach aussen und nach innen offen sind und können für mehrere Instanzen gleichzeitig eingerichtet werden.

*Key Pairs* sind Private & Public Keys. Wobei der Public Key bei Amazon verbleibt und der Private Key vom User lokal abgelegt wird um damit auf die VMs in der Cloud zugreifen zu können. 

**AWS Images** <br>
Es gibt vorbereitete VM-Images von AWS, welche einfach über die EC2-Konsole instanziert werden können.

<br>

![](../images/azure_36x36.png "Microsoft Azure Cloud") 04 - Azure Cloud
======

> [⇧ **Nach oben**](#inhaltsverzeichnis)

### Grundlagen
***

Microsoft Azure ist eine branchenführende Cloud-Plattform, die eine umfassende Suite von Cloud-Services und -Tools für Unternehmen bereitstellt. Ähnlich wie AWS bietet Azure eine Vielzahl von Diensten, darunter 
- Computing
- Speicherung
- Datenbanken
- künstliche Intelligenz
- IoT 

...und vieles mehr. Azure ermöglicht es Unternehmen, ihre Anwendungen und Workloads in der Cloud zu betreiben und von der Flexibilität, Skalierbarkeit und Zuverlässigkeit der Cloud-Infrastruktur zu profitieren.

Azure bietet ebenfalls wie AWS eine Vielzahl von Diensten, darunter 
- Azure Virtual Machines
- Azure App Service
- Azure SQL Database
- Azure Blob Storage
- Azure Functions

...um nur einige zu nennen. Diese Dienste ermöglichen es Unternehmen, ihre Anwendungen schnell zu entwickeln, bereitzustellen und zu skalieren, während sie gleichzeitig die Infrastruktur effizient verwalten und optimieren können.

Darüber hinaus bietet Azure eine umfassende Suite von Tools und Diensten für Sicherheit, Compliance und Überwachung, um die Daten und Anwendungen der Kunden zu schützen. Mit Azure können Unternehmen ihre digitale Transformation vorantreiben und innovative Lösungen entwickeln, um wettbewerbsfähig zu bleiben und ihre Geschäftsziele zu erreichen.


<br>

![](../images/Reflexion_36x36.png) 05 - Wissenswert
======

> [⇧ **Nach oben**](#inhaltsverzeichnis)

Unter **[Cloud Computing](https://de.wikipedia.org/wiki/Cloud_Computing)** versteht man die Ausführung von Programmen, die nicht auf dem lokalen Rechner installiert sind, sondern auf einem anderen Rechner, der aus der Ferne aufgerufen wird (bspw. über das Internet).

Eine **dynamische Infrastruktur-Plattform** ist ein System, das Rechen-Ressourcen bereitstellt (Virtualisiert),
insbesondere Server (compute), Speicher (storage) und Netzwerke (networks), und diese
Programmgesteuert zuweist und verwaltet, sogenannte **Virtuelle Maschinen** (VM).

Damit "Infrastructure as Code" auf "Dynamic Infrastructure Platforms" genutzt werden können, müssen sie die folgenden Anforderungen erfüllen:

- **Programmierbar** - Ein Userinterface ist zwar angenehm und viele Cloud Anbieter haben eines, aber für "Infrastructure as Code"
muss die Plattform via Programmierschnittstelle ([API](https://de.wikipedia.org/wiki/Programmierschnittstelle)) ansprechbar sein.
- **On-demand** - Ressourcen (Server, Speicher, Netzwerke) schnell erstellen und vernichtet.
- **Self-Service** - Ressourcen anpassen und auf eigene Bedürfnisse zuschneiden.
- **Portabel** - Anbieter von Ressourcen müssen austauschbar sein.
- Sicherheit, Zertifizierungen (z.B. [ISO 27001](https://de.wikipedia.org/wiki/ISO/IEC_27001)), ...


<br>

---

> [⇧ **Nach oben**](#lernziele)

---

> [⇧ **Zurück zur Hauptseite**](../README.md)


___

