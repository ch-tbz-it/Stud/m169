<!--
Vorlage für das Aufklappen
:question: Text
<details><summary>:white_check_mark: Antwort</summary>

 Text
</p></details>

---
-->

# Fragen zum Kapitel 35

## Protokollieren & Überwachen

:question: Warum sollten Container überwacht werden?
<details><summary>:white_check_mark: Antwort</summary>
 
 Um Fehler oder grosse Ressourcenbelastungen frühzeitig zu erkennen und einzugreifen	
</p></details>

---

:question: Was ist das syslog und wo ist es zu finden?
<details><summary>:white_check_mark: Antwort</summary> 

 Das Systemweite Log eines Linux Hosts. Verzeichnis /var/log.
</p></details>
	
---

:question: Was ist stdout, stderr, stdin?
<details><summary>:white_check_mark: Antwort</summary>

 Standard Output, Standard Error Ausgabe und Standard Input Eingabe. 	
</p></details>

---

## Container sichern & beschränken

:question: Wie kann `docker run -v /:/homeroot -it ubuntu bash` durch Normale User verhindert werden?
<details><summary>:white_check_mark: Antwort</summary>

 nur `root` darf Container starten
</p></details>

---

:question: Wie können verschiedene Mandanten (tenants) getrennt werden?
<details><summary>:white_check_mark: Antwort</summary> 

 **V1**: Mittels VM's

 >:heavy_plus_sign: Starke Isolation, Sicherheit, anpassbare OS-Umgebung, weit verbreiteter Lösungsansatz

 >:heavy_minus_sign: Weniger effiziente Ressourcennutzung, Komplexität im Management / Automatisierung, zeitaufwendigere Bereitstellung

 **V2**: Namensräume definieren (Namespaces)

 >Systemressourcen sind so zu partitionieren, dass Prozesse nur auf ihre eigenen Ressourcenansichten zugreifen können. In Bezug auf Container ermöglichen Namensräume die Isolation von Prozessen, Netzwerken, Benutzern und Dateisystemen. Jeder Container läuft in einem eigenen Satz von Namensräumen, wodurch die Trennung zwischen den Containern unterschiedlicher Mandanten gewährleistet wird.

 **V3**: Kontrollgruppen definieren

 >Kontrollgruppen sind ein weiteres Feature des Linux-Kernels, das zur Ressourcenverwaltung eingesetzt wird. Sie erlauben es, die Ressourcennutzung (wie CPU, Speicher, Netzwerkbandbreite, etc.) von Prozessgruppen zu überwachen und zu beschränken. Durch die Zuweisung von Containern zu spezifischen cgroups kann sichergestellt werden, dass ein Mandant die Ressourcen eines anderen nicht überbeanspruchen kann.

 **V4**: Container-Netzwerk-Policies / Security Contexts

 >Durch die Definition von Netzwerk-Policies kann der Netzwerkverkehr zwischen Containern unterschiedlicher Mandanten kontrolliert und isoliert werden.

 **V5**: Pod Scurity Policies (PSPs)

 >Zugriff auf Host-Systeme kontrollieren und die Verwendung von Benutzer- und Gruppen-IDs steuern.

 **V6**: Service Meshes

 > Ein Service Mesh bietet eine zusätzliche Ebene der Abstraktion und Kontrolle für die Kommunikation zwischen Diensten in einem Container-Cluster. 

 **V7**: Storage Isolation

 > Durch die Verwendung von persistenten Volumes (PVs) und persistenten Volume Claims (PVCs) sichergestellt werden, dass Daten zwischen Mandanten isoliert und geschützt bleiben.

 Die Wahl der Strategie zur Trennung verschiedener      Mandanten in einer containerbasierten Plattform hängt von den spezifischen (Kunden-)Anforderungen an die Isolation, Sicherheit und Skalierbarkeit ab. In der Praxis wird für das Design der Plattform oft eine Kombination der beschriebenen Varianten eingesetzt, um eine robuste und sichere Multi-Tenant-Umgebung zu schaffen.
</p></details>
	
---

:question: Wie kann der Ressourcenverbrauch von Containern eingeschränkt werden?
<details><summary>:white_check_mark: Antwort</summary> 

 https://docs.docker.com/config/containers/resource_constraints/#configure-the-default-cfs-scheduler 
</p></details>

---

## Kontinuierliche Integration

:question: Welche Funktionen kann Jenkins übernehmen?
<details><summary>:white_check_mark: Antwort</summary>

 CI, Modultests, Software bauen, Batch Jobs ausführen - z.B. Logs überprüfen
</p></details>

---

:question: Wie baut man Modultests?
<details><summary>:white_check_mark: Antwort</summary> 

 Via Bash Scripts
</p></details>
	
---

:question: Wie anders, als Manuell oder Zeitgesteuert könnten Jenkins Jobs auch gestartet werden?
<details><summary>:white_check_mark: Antwort</summary>

 Durch Änderungen in einem Git Repository
</p></details>
