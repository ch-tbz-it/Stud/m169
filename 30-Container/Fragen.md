<!--
Vorlage für das Aufklappen
:question: Text
<details><summary>:white_check_mark: Antwort</summary>

 Text
</p></details>

---
-->

# Fragen zum Kapitel 30

## Container

:question: Was ist der Unterschied zwischen Vagrant und Docker?
<details><summary>:white_check_mark: Antwort</summary>

 Vagrant ist für IaaS (Infrastructure as a Service / Virtuelle Maschinen) und Docker für CaaS (Container as a Service). Eine Plattform, die den Betrieb von Docker-Containern unterstützt, wird dann als PaaS (Platform as a Service) bezeichnet. 
</p></details>

---

:question: Was welches Tools aus dem Docker Universum ist Vergleichbar mit Vagrant?
<details><summary>:white_check_mark: Antwort</summary>

 [docker machine](https://thinkster.io/tutorials/docker-fundamentals-installing-docker/docker-machine)
</p></details>

---

:question: Was macht der Docker Provisioner von Vagrant?
<details><summary>:white_check_mark: Antwort</summary>

 Installiert Docker in einer VM
</p></details>

---

:question: Welche Linux Kernel Funktionalität verwenden Container?
<details><summary>:white_check_mark: Antwort</summary>

 Linux Namespaces, siehe auch [The Missing Introduction To Containerization](https://medium.com/faun/the-missing-introduction-to-containerization-de1fbb73efc5)
</p></details>

---

:question: Welches Architekturmuster verwendet der Entwickler wenn er Container einsetzt?
<details><summary>:white_check_mark: Antwort</summary>

 Microservices
</p></details>

---

:question: Welches sind die drei Hauptmerkmale (abgeleitet vom Ur-Unix) von Microservices?
<details><summary>:white_check_mark: Antwort</summary>

 Ein Programm soll nur eine Aufgabe erledigen, und das soll es gut machen. Programme sollen zusammenarbeiten können. Nutze eine universelle Schnittstelle. In UNIX sind das Textströme. Bei Microservices das Internet (REST).
</p></details>

---

## Docker

:question: Was ist der Unterschied zwischen einem Docker Image und einem Container?
<details><summary>:white_check_mark: Antwort</summary>

 Image = gebuildet und readonly, Container Image + aktuelle Änderungen im Filesystem
</p></details>

---

:question: Was ist der Unterschied zwischen einer Virtuellen Maschine und einem Docker Container?
<details><summary>:white_check_mark: Antwort</summary>

 VM hat Betriebssystem mit am laufen, Docker nur die eigenen Prozesse
</p></details>
	
---

:question: Wie bekomme ich Informationen zu einem laufenden Docker Container?
<details><summary>:white_check_mark: Antwort</summary>

 docker logs, docker inspect
</p></details>

---

:question: Was ist der Unterschied zwischen einer Docker Registry und einem Repository
<details><summary>:white_check_mark: Antwort</summary>

 In der Docker Registry werden die Container Images gespeichert. Ein Repository speichert pro Container Image verschiedene Versionen von Images.
</p></details>

---

:question: Wie erstelle ich ein Container Image
<details><summary>:white_check_mark: Antwort</summary>

 docker build 
</p></details>

---

:question: In welcher Datei steht welche Inhalte sich im Container Image befinden?
<details><summary>:white_check_mark: Antwort</summary>

 Dockerfile 
</p></details>

---

:question: Der erste Prozess im Container bekommt die Nummer?
<details><summary>:white_check_mark: Antwort</summary>

 1 
</p></details>

---

:question: Welche Teile von Docker sind durch Kubernetes obsolet geworden, bzw. sollten nicht mehr verwendet werden?
<details><summary>:white_check_mark: Antwort</summary>

 Swarm, Compose, Network, Volumes
</p></details>

---

:question: Welche Aussage ist besser (siehe auch [The Twelve-Factor App](https://12factor.net/))?
* a) Dockerfile sollten möglichst das Builden (CI) und Ausführen von Services beinhalten, so ist alles an einem Ort und der Entwickler kann alles erledigen.
* b) Das Builden und Ausführen von Services ist strikt zu trennen. Damit saubere und nachvollziehbare Services mittels CI/CD Prozess entstehen. 
<details><summary>:white_check_mark: Antwort</summary>

 b)
</p></details>

---

:question: Was sind die Eigenschaften von einem Dockerfile?
<details><summary>:white_check_mark: Antwort</summary>

 Ein Docker-Container-Konfigurationsfile, oft als Dockerfile bezeichnet, hat keine Dateiendung. Es wird einfach als "Dockerfile" (ohne Punkt und Endung) benannt und enthält alle notwendigen Befehle, die von Docker gelesen werden, um ein Image zu erstellen.
</p></details>

---

## Docker Hub

:question: Was ist Docker Hub?
<details><summary>:white_check_mark: Antwort</summary>

 Ein Container Registry, wo Container Image gespeichert werden. Docker Hub wird durch die Firma Docker zur Verfügung gestellt wird.
</p></details>

---

:question: Welches sind die Alternativen?
<details><summary>:white_check_mark: Antwort</summary>

 Praktisch jeder Cloud Anbieter stellt eine Container Registry zur Verfügung. Auch die Anbieter für die Verwaltung von Build Artefakten (z.B. Sonatype Nexus) stellen Docker Registries zur Verfügung oder haben deren Funktionalität integriert. 
</p></details>

---

:question: Warum sollte eine eigene Docker Registry im Unternehmen verwendet werden?
<details><summary>:white_check_mark: Antwort</summary>

 Sicherheit, bzw. das mögliche Fehlen davon. Es kann nicht Sichergestellt werden, dass alle Container Images auf Docker Hub sicher sind.
</p></details>

---

:question: Warum sollten Versionen `tag` von Images immer angegeben werden?
<details><summary>:white_check_mark: Antwort</summary>

 Ansonsten wird `latest` verwendet und so nicht sicher welche Version wirklich verwendet wird.
</p></details>

---

:question: Was ist der Unterschied zwischen `docker save`/`docker load` und `docker export`/`docker import`?
<details><summary>:white_check_mark: Antwort</summary>

 save/load ist für Images, export/import für Container.
 So ist es möglich auch ohne Docker Registry Container Images auszutauschen, z.B. in einer Bank.
</p></details>

---

:question: Hat ein Docker-Container eine IP-Adresse?
<details><summary>:white_check_mark: Antwort</summary>

 Ja, ein Docker-Container kann eine IP-Adresse haben. In Docker werden Container in einem isolierten Netzwerk ausgeführt, und jedem Container kann eine eindeutige IP-Adresse zugewiesen werden, um die Kommunikation mit anderen Containern im selben Netzwerk oder mit dem Host-System zu ermöglichen. Die Zuweisung der IP-Adresse hängt von der Konfiguration des Docker-Netzwerks ab, in dem der Container läuft.
 
 Es gibt verschiedene Netzwerkmodi in Docker, darunter: **Bridge-Netzwerk** (Standard-Netzwerkmodus für Docker-Container), **Host-Netzwerk** (IP von Docker-Host wird verwendet), **Overlay-Netzwerk** (Docker Swarm-Umgebungen, jeder Container erhält eine eigene IP-Adresse im Overlay-Netzwerk)
 Beispielbefehl zur Ermittlung der IP-Adresse eines Containers:

 `docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container_name_or_id `
 
 [How to Get A Docker Container IP Address - Explained with Examples](https://www.freecodecamp.org/news/how-to-get-a-docker-container-ip-address-explained-with-examples/)
</p></details>

---

## Fachbegriffe


:question: Was versteht man unter dem Begriff ***Persistenz***?
<details><summary>:white_check_mark: Antwort</summary>

 Persistenz bezieht sich auf die Fähigkeit von Daten, über einen längeren Zeitraum hinweg beständig und dauerhaft zu bleiben, auch über das Ende von Prozessen, Systemneustarts oder das Löschen von Containern hinaus. In der Welt der Docker-Containerisierung wird die Persistenz von Daten durch die Verwendung von Volumes erreicht. Volumes sind spezielle Bereiche auf der Host-Maschine, die ausserhalb der Container existieren und an diese angebunden werden können. Sie ermöglichen es, dass Daten unabhängig vom Lebenszyklus der Container erhalten bleiben, was für Anwendungen, die dauerhafte Daten benötigen (wie Datenbanken), unerlässlich ist.
</p></details>

---

:question: Was versteht man unter dem Begriff ***Containerlebenszyklus***?
<details><summary>:white_check_mark: Antwort</summary>

 Der Containerlebenszyklus beschreibt die verschiedenen Phasen, die ein Docker-Container von seiner Erstellung bis zu seiner Entfernung durchläuft. Der Zyklus beginnt mit der Definition eines Dockerfiles, das die Anweisungen zur Erstellung eines Docker-Images enthält. Nachdem das Image erstellt wurde, kann ein Container aus diesem Image initiiert werden. Dieser Prozess umfasst das Erstellen, Starten, Anhalten, Fortsetzen und schliesslich das Löschen des Containers. Während seines aktiven Zustands kann der Container verschiedene Zustände wie "Laufen", "Gestoppt" und "Pause" einnehmen. Die Konfiguration des Containers wird durch das Dockerfile und die beim Start des Containers spezifizierten Optionen festgelegt, welche Aspekte wie Umgebungsvariablen, Netzwerk-Einstellungen und angebundene Volumes umfassen können. Änderungen, die innerhalb des laufenden Containers an dessen Dateisystem gemacht werden und nicht in Volumes oder anderen Persistenzmechanismen gespeichert sind, gehen mit dem Löschen des Containers verloren. Volumes werden verwendet, um Daten zu speichern, die über den Lebenszyklus des Containers hinaus bestehen bleiben sollen, während die Containerkonfiguration die Ausführungsbedingungen und das Verhalten des Containers definiert.
</p></details>

---

:question: Was versteht man unter dem Begriff ***Ökosystem*** (mit Bezug zu Cloud-Technologien)?
<details><summary>:white_check_mark: Antwort</summary>
 
 Ein Ökosystem in Cloud-Technologien bezeichnet eine integrierte Plattform, die aus miteinander verbundenen Diensten, (Infrastrukturdiensten bis hin zu Entwicklertools und Softwareanwendungen) besteht. Es bietet klare Schnittstellen für die Entwicklung, Bereitstellung und das Management von Anwendungen und Diensten in der Cloud. Diese Plattformen unterstützen Innovation und Effizienz wobei Sicherheits- und Compliance-Aspekte berücksichigt werden (Security by default).
</p></details>

---

