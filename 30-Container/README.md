# M169 - 30 Container 

[TOC]


## Verortete Lernziele in der Kompetenzmatrix mit Erklärungen

**Verständnis des Container-Ökosystems und der Architektur**
- **A1F**: Ich kann Eigenschaften von Containern in eigenen Worten beschreiben und bestehende Container einsetzen.
- **A1E**: Ich kann eine Container Umgebung mit einem oder mehreren Services erläutern.

>Dieses Ziel fördert ein tiefes Verständnis der Container-Technologie und ihrer Architektur, was über die Grundlagen hinausgeht und in das fortgeschrittene Verständnis und die Erläuterung einer Container-Umgebung mündet.
---
**Effiziente Container-Orchestrierung und Management**
- **H1F**: Ich kann einen oder mehrere Services in Containern bereitstellen und verwende geeignete Mittel zur Behebung von Fehlern.
- **I1F**: Ich kann zur Überwachung und Administration von Services, deren Ressourcenverwendung nachvollziehen und überwachen.

>Dieses Ziel erweitert die Fähigkeiten im Bereich der Orchestrierung und des Managements von Containern, was eine fortgeschrittene Kompetenz darstellt. Es geht darum, Container nicht nur bereitzustellen, sondern auch effizient zu verwalten und zu überwachen.
---
**Sicherheitspraktiken für Container-Anwendungen**
- **F1F**: Ich kann Sicherheitsanforderungen umsetzen und auf Ihre Wirksamkeit prüfen.
- **F1E**: Ich kann bei schwachen Sicherheitsanforderungen mit zusätzlichen, transparenten Massnahmen die Sicherheit erhöhen.

>Dieses Ziel konzentriert sich auf die Implementierung von Sicherheitsmaßnahmen in Container-Umgebungen. Es zeigt eine fortgeschrittene bis erweiterte Kompetenz in der Fähigkeit, Sicherheitsanforderungen nicht nur zu erkennen und umzusetzen, sondern auch die Sicherheit proaktiv zu erhöhen.
---
**Sie können Applikationen und Services als Container betreiben und als Container Images für Dritte zur Verfügung stellen**
- **H1F**: Ich kann einen oder mehrere Services in Containern bereitstellen und verwende geeignete Mittel zur Behebung von Fehlern.
- **H1E**: Ich kann mit der Nutzung der Registry Container Images wiederverwenden und kann den Ablauf, um eigene Images in der Registry verfügbar zu machen, beschreiben.

>Dieses Ziel ermöglicht es Lernenden, die praktische Anwendung von Container-Technologien zu meistern, indem es nicht nur die Bereitstellung und das Management von Containern und Services umfasst, sondern auch die Fähigkeit vermittelt, Container-Images effektiv für die Nutzung durch Dritte zur Verfügung zu stellen. Durch das Erlernen, wie man Container-Images in einer Registry verwaltet und teilt, gewinnen die Lernenden Einblicke in die Bedeutung der Wiederverwendbarkeit und Portabilität in der modernen Softwareentwicklung. Dies fördert ein umfassendes Verständnis des Ökosystems der Containerisierung und bereitet die Lernenden auf die Herausforderungen und Best Practices im Bereich der Softwarebereitstellung und -verteilung vor. Es unterstreicht die Wichtigkeit der Collaboration und des Teilens von Ressourcen in Entwicklergemeinschaften, um effiziente, skalierbare und wartbare Anwendungen zu erstellen.

## Voraussetzungen

* [10 Toolumgebung](../10-Toolumgebung/)


# ![](../images/Ship_36x36.png "Container") 01 - Container

Container ändern die Art und Weise, wie wir Software und Services entwickeln, verteilen und laufen lassen, grundlegend.

Entwickler können Software lokal bauen, die woanders genauso laufen wird – sei es ein Rack in der IT-Abteilung, der Laptop eines Anwenders oder ein Cluster in der Cloud, weil sie in eine isolierte Umgebung gepackt wird.

Container beeinflussen nicht nur die Softwareentwicklung, sondern auch die Art und Weise, wie Services bereitgestellt und verwaltet werden, insbesondere in Cloud-Umgebungen. Administratoren können sich auf die Netzwerke, Ressourcen und die Uptime konzentrieren und müssen weniger Zeit mit dem Konfigurieren von Umgebungen und dem Kampf mit Systemabhängigkeiten verbringen.

**Merkmale** <br>
* Container teilen sich Ressourcen mit dem Host-Betriebssystem
* Container können im Bruchteil einer Sekunde gestartet und gestoppt werden
* Anwendungen, die in Containern laufen, verursachen wenig bis gar keinen Overhead
* Container sind portierbar --> Fertig mit "Aber bei mir auf dem Rechner lief es doch!"
* Container sind leichtgewichtig, d.h. es können dutzende parallel betrieben werden.
* Container sind "Cloud-ready"!


### Geschichte
***
Container sind ein altes Konzept. Schon seit Jahrzehnten gibt es in UNIX-Systemen den Befehl chroot, der eine einfache Form der Dateisystem-Isolation bietet.

Seit 1998 gibt es in FreeBSD das Jail-Tool, welches das chroot-Sandboxing auf Prozesse erweitert.

Solaris Zones boten 2001 eine recht vollständige Technologie zum Containerisieren, aber diese war auf Solaris OS beschränkt.

Ebenfalls 2001 veröffentlichte Parallels Inc. (damals noch SWsoft) die kommerzielle Containertechnologie Virtuozzo für Linux, deren Kern später (im Jahr 2005) als Open Source unter dem Namen OpenVZ bereitgestellt wurde.

Dann startete Google die Entwicklung von CGroups für den Linux-Kernel und begann damit, seine Infrastruktur in Container zu verlagern.

Das Linux Containers Project (LXC) wurde 2008 initiiert, und in ihm wurden (unter anderem) CGroups, Kernel-Namensräume und die chroot-Technologie zusammengeführt, um eine vollständige Containerisierungslösung zu bieten.

2013 lieferte Docker schliesslich die fehlenden Teile für das Containerisierungspuzzle, und die Technologie begann den Mainstream zu erreichen.

Siehe auch [The Missing Introduction To Containerization](https://medium.com/devopslinks/the-missing-introduction-to-containerization-de1fbb73efc5)

### Microservices
***

[![](https://img.youtube.com/vi/PH4HtZ8naWs/0.jpg)](https://www.youtube.com/watch?v=PH4HtZ8naWs)

Microservices YouTube Einführung

---

Einer der grössten Anwendungsfälle und die stärkste treibende Kraft hinter dem Aufstieg von Containern sind Microservices.

Microservices sind ein Weg, Softwaresysteme so zu entwickeln und zu kombinieren, dass sie aus kleinen, unabhängigen Komponenten bestehen, die untereinander über das Netz interagieren. Das steht im Gegensatz zum klassischen, monolithischen Weg der Softwareentwicklung, bei dem es ein einzelnes, grosses Programm gibt.

Wenn solch ein Monolith* dann skaliert werden muss, kann man sich meist nur dazu entscheiden, vertikal zu skalieren (scale up), zusätzliche Anforderungen werden in Form von mehr RAM und mehr Rechenleistung bereitgestellt. Microservices sind dagegen so entworfen, dass sie horizontal skaliert werden können (scale out), indem zusätzliche Anforderungen durch mehrere Rechner verarbeitet werden, auf die die Last verteilt werden kann.

In einer Microservices-Architektur ist es möglich, nur die Ressourcen zu skalieren, die für einen bestimmten Service benötigt werden, und sich damit auf die Flaschenhälse des Systems zu beschränken. In einem Monolith wird alles oder gar nichts skaliert, was zu verschwendeten Ressourcen führt.

>* Monolith = Monolithische Applikationen oder Services, die den Microservice-Ansatz nicht erfüllen, zeichnen sich durch ihre integrierte Struktur aus, in der verschiedene Funktionen und Dienste eng miteinander verwoben sind. 
Hier einige konkrete Beispiele: **ERP-Systeme** wie SAP, **E-Commerce-Plattformen** wie Magento, **Content Management Systeme (CMS)** wie WordPress oder Joomla, **Microsoft Exchange Server**.


# ![](../images/Docker_36x36.png?raw=true "Docker") 02 - Docker


> [⇧ **Nach oben**](#m169---30-container)

[![](https://img.youtube.com/vi/YFl2mCHdv24/0.jpg)](https://www.youtube.com/watch?v=YFl2mCHdv24)

Docker YouTube Einführung

---

Docker nahm die bestehende Linux-Containertechnologie auf und verpackte und erweiterte sie in vielerlei Hinsicht – vor allem durch portable Images und eine benutzerfreundliche Schnittstelle –, um eine vollständige Lösung für das Erstellen und Verteilen von Containern zu schaffen.

Die Docker-Plattform besteht vereinfacht gesagt aus zwei getrennten Komponenten: der Docker Engine, die für das Erstellen und Ausführen von Containern verantwortlich ist, sowie dem Docker Hub, einem Cloud Service, um Container-Images zu verteilen.

**Wichtig:** Docker wurde für 64-bit Linux Systeme entwickelt, kann jedoch auch mittels VirtualBox auf Mac und Windows betrieben werden.

### Architektur
***

Nachfolgend sind die wichtigsten Komponenten von Docker aufgelistet:

**Docker Deamon** <br>
* Erstellen, Ausführen und Überwachen der Container
* Bauen und Speichern von Images

Der Docker Daemon wird normalerweise durch das Host-Betriebssystem gestartet.

**Docker Client** <br> 
* Docker wird über die Kommandozeile (CLI) mittels des Docker Clients bedient
* Kommuniziert per HTTP REST mit dem Docker Daemon

Da die gesamte Kommunikation über HTTP abläuft, ist es einfach, sich mit entfernten Docker Daemons zu verbinden und Bindings an Programmiersprachen zu entwickeln.

**Images** <br> 
* Images sind gebuildete Umgebungen welche als Container gestartet werden können
* Images sind nicht veränderbar, sondern können nur neu gebuildet werden.
* Images bestehen aus Namen und Version (TAG), z.B. *ubuntu:16.04.* 
    * Wird keine Version angegeben wird automatisch :latest angefügt.

**Container** <br> 
* Container sind die ausgeführten Images
* Ein Image kann beliebig oft als Container ausgeführt werden
* Container bzw. deren Inhalte können verändert werden, dazu werden sogenannte *Union File Systems* verwendet, welche nur die Änderungen zum original Image speichern.

**Docker Registry** <br> 
* In Docker Registries werden Images abgelegt und verteilt

Die Standard-Registry ist der Docker Hub, auf dem tausende öffentlich verfügbarer Images zur Verfügung stehen, aber auch "offizielle" Images.

Viele Organisationen und Firmen nutzen eigene Registries, um kommerzielle oder "private" Images zu hosten, aber auch um den Overhead zu vermeiden, der mit dem Herunterladen von Images über das Internet einhergeht. 


### Befehle
***
Der Docker Client bietet eine Vielzahl von Befehlen, die für die Bedienung der Anwendung genutzt werden können. In diesem Abschnitt werden daher jene Befehle etwas näher beleuchtet.

**docker run** <br>
* Ist der Befehl zum Starten neuer Container.
* Der bei weitem komplexesten Befehl, er unterstützt eine lange Liste möglicher Argumente.
* Ermöglicht es dem Anwender, zu konfigurieren, wie das Image laufen soll, Dockerfile-Einstellungen zu überschreiben, Verbindungen zu konfigurieren und Berechtigungen und Ressourcen für den Container zu setzen.

Standard-Test:
```Shell
    $ docker run hello-world
```

Startet einen Container mit einer interaktiven Shell (interactive, tty):
```Shell
    $ docker run -it ubuntu /bin/bash
```

Startet einen Container, der im Hintergrund (detach) läuft:
```Shell
    $ docker run -d ubuntu sleep 20
```

Startet einen Container im Hintergrund und löscht (remove) diesen nach Beendigung des Jobs:
```Shell
    $ docker run -d --rm ubuntu sleep 20
```

Startet einen Container im Hintergrund und legt eine Datei an:
```Shell
    $ docker run -d ubuntu touch /tmp/lock
```

Startet einen Container im Hintergrund und gibt das ROOT-Verzeichnis (/) nach STDOUT aus:
```Shell
    $ docker run -d ubuntu ls -l
```

**docker ps** <br>
* Gibt einen Überblick über die aktuellen Container, wie z.B. Namen, IDs und Status.

Aktive Container anzeigen:
```Shell
    $ docker ps
```

Aktive und beendete Container anzeigen (all):
```Shell
    $ docker ps -a
```

Nur IDs ausgeben (all, quit):
```Shell
    $ docker ps -a -q
```

**docker images** <br>
* Gibt eine Liste lokaler Images aus, wobei Informationen zu Repository-Namen, Tag-Namen und Grösse enthalten sind.

Lokale Images ausgeben:
```Shell
    $ docker images
```

Alternativ auch mit `... image ls`:
```Shell
    $ docker image ls
```

**docker rm und docker rmi** <br>
* `docker rm`
    *  Entfernt einen oder mehrere Container. Gibt die Namen oder IDs erfolgreich gelöschter Container zurück.
* `docker rmi`
    *  Löscht das oder die angegebenen Images. Diese werden durch ihre ID oder Repository- und Tag-Namen spezifiziert.

Docker Container löschen:
```Shell
    $ docker rm [name]
```

Alle beendeten Container löschen:
```Shell
    $ docker rm `docker ps -a -q`
```

Alle Container, auch aktive, löschen:
```Shell
    $ docker rm -f `docker ps -a -q`
```

Docker Image löschen:
```Shell
    $ docker rmi ubuntu
```

Zwischenimages löschen (haben keinen Namen):
```Shell
    $ docker rmi `docker images -q -f dangling=true`
```

**docker start** <br>
* Startet einen (oder mehrere) gestoppte Container. 
    * Kann genutzt werden, um einen Container neu zu starten, der beendet wurde, oder um einen Container zu starten, der mit `docker create` erzeugt, aber nie gestartet wurde.

Docker Container neu starten, die Daten bleiben erhalten:
```Shell
    $ docker start [id]
```

**Container stoppen, killen** <br>
* `docker stop`
    * Stoppt einen oder mehrere Container (ohne sie zu entfernen). Nach dem Aufruf von `docker stop` für einen Container wird er in den Status »exited« überführt.
* `docker kill`
    * Schickt ein Signal an den Hauptprozess (PID 1) in einem Container. Standardmässig wird SIGKILL gesendet, womit der Container sofort stoppt.

**Informationen zu Containern** <br>
* `docker logs`
    * Gibt die "Logs" für einen Container aus. Dabei handelt es sich einfach um alles, was innerhalb des Containers nach STDERR oder STDOUT geschrieben wurde.
* `docker inspect`
    * Gibt umfangreiche Informationen zu Containern oder Images aus. Dazu gehören die meisten Konfigurationsoptionen und Netzwerkeinstellungen sowie Volumes-Mappings.
* `docker diff`
    * Gibt die Änderungen am Dateisystem des Containers verglichen mit dem Image aus, aus dem er gestartet wurde.
* `docker top`
    * Gibt Informationen zu den laufenden Prozessen in einem angegebenen Container aus.


### Dockerfile
***
Ein Dockerfile ist eine Textdatei mit einer Reihe von Schritten, die genutzt werden können, um ein Docker-Image zu erzeugen.

Dazu wird zuerst ein Verzeichnis erstellt und darin eine Datei mit Namen "Dockerfile". 

>>>
:information_source: Dockerfile
Docker-Container-Konfigurationsfiles (Dockerfile) hat **keine** Dateiendung. Es wird einfach als "Dockerfile" (ohne Punkt und Endung) benannt und enthält alle notwendigen Befehle, die von Docker gelesen werden, um ein Image zu erstellen.

[Dockerfile reference](https://docs.docker.com/reference/dockerfile/)
>>>

Anschliessend kann das Image wie folgt gebuildet werden:
```Shell
    $ docker build -t mysql .
```

Starten:
```Shell
    $ docker run --rm -d --name mysql mysql
```

Funktionsfähigkeit überprüfen:
```Shell
    $ docker exec -it mysql bash
```

Überprüfung im Container:
```Shell
    $ ps -ef
    $ netstat -tulpen
```

### Konzepte
***

**Build Context** <br>
* Der Befehl `docker build` erfordert ein Dockerfile und einen Build Context.
    * Der Build Context ist der Satz lokaler Dateien und Verzeichnisse, die aus ADD- oder COPY-Anweisungen im Dockerfile angesprochen werden können.
    * Er wird im Allgemeinen als Pfad zu einem Verzeichnis definiert.

**Layer / Imageschichten** <br>
* Jede Anweisung in einem Dockerfile führt zu einer neuen Imageschicht – einem Layer –, die wieder zum Starten eines neuen Containers genutzt werden kann.
* Die neue Schicht wird erzeugt, indem ein Container mit dem Image der vorherigen Schicht gestartet, dann die Dockerfile-Anweisung ausgeführt und schliesslich ein neues Image gespeichert wird.
* Ist eine Dockerfile-Anweisung erfolgreich abgeschlossen worden, wird der temporär erzeugte Container wieder gelöscht.

**Anweisungen im Dockerfile** <br>

* `FROM`
    * Welches Base Image von [hub.docker.com](https://hub.docker.com) verwendet werden soll, z.B. ubuntu:16.04
* `ADD`
    *  Kopiert Dateien aus dem Build Context oder von URLs in das Image.
* `CMD`
    * Führt die angegebene Anweisung aus, wenn der Container gestartet wurde. Ist auch ein ENTRYPOINT definiert, wird die Anweisung als Argument für ENTRYPOINT verwendet.
* `COPY`
    * Wird verwendet, um Dateien aus dem Build Context in das Image zu kopieren. Es gibt die zwei Formen COPY src dest und COPY ["src", "dest"]. Das JSON-Array-Format ist notwendig, wenn die Pfade Leerzeichen enthalten.
* `ENTRYPOINT`
    * Legt eine ausführbare Datei (und Standardargumente) fest, die beim Start des Containers laufen soll. 
    * Jegliche CMD-Anweisungen oder an `docker run` nach dem Imagenamen übergebenen Argumente werden als Parameter an das Executable durchgereicht. 
    * ENTRYPOINT-Anweisungen werden häufig genutzt, um "Start-Scripts" anzustossen, die Variablen und Services initialisieren, bevor andere übergebene Argumente ausgewertet werden.
* `ENV`
    * Setzt Umgebungsvariablen im Image.
* `EXPOSE`
    * Erklärt Docker, dass der Container einen Prozess enthält, der an dem oder den angegebenen Port(s) lauscht.
* `HEALTHCHECK`
    * Die Docker Engine prüft regelmässig den Status der Anwendung im Container.
        ```Shell 
            HEALTHCHECK --interval=5m --timeout=3s \ CMD curl -f http://localhost/ || exit 1`
        ```
* `MAINTAINER` 
    * Setzt die "Autor-Metadaten" des Image auf den angegebenen Wert.
* `RUN` 
    * Führt die angegebene Anweisung im Container aus und bestätigt das Ergebnis.
* `SHELL` 
    * Die Anweisung SHELL erlaubt es seit Docker 1.12, die Shell für den folgenden RUN-Befehl zu setzten. So ist es möglich, dass nun auch direkt bash, zsh oder Powershell-Befehle in einem Dockerfile genutzt werden können.
* `USER` 
    * Setzt den Benutzer (über Name oder UID), der in folgenden RUN-, CMD- oder ENTRYPOINT-Anweisungen genutzt werden soll.
* `VOLUME` 
    * Deklariert die angegebene Datei oder das Verzeichnis als Volume. Besteht die Datei oder das Verzeichnis schon im Image, wird sie bzw. es in das Volume kopiert, wenn der Container gestartet wird.
* `WORKDIR` 
    * Setzt das Arbeitsverzeichnis für alle folgenden RUN-, CMD-, ENTRYPOINT-, ADD oder COPY-Anweisungen.


# ![](../images/Network_36x36.png?raw=true "Netzwerk-Anbindung") 03 - Netzwerk-Anbindung

> [⇧ **Nach oben**](#m169---30-container)

Stellen Sie sich vor, Sie lassen einen Webserver in einem Container laufen. Wie können Sie dann der Aussenwelt darauf Zugriff gewähren?

Die Antwort ist, Ports mit den Befehlen -p oder -P zu "veröffentlichen". Dieser Befehl leitet Ports auf den Host des Containers weiter.

**Beispiele** <br>

MySQL Container permanent an Host Port 3306 weiterleiten:
```Shell
    $ docker run --rm -d -p 3306:3306 mysql
```

MySQL Container mit nächsten freien Port verbinden:
```Shell
    $ docker run --rm -d -P mysql
```

**Erweiterung Dockerfile** <br>
Um Ports an den Host bzw. das Netzwerk weiterzuleiten, sind diese im Dockerfile via EXPOSE einzutragen.

Beispiel MySQL-Standardport:
```Shell
    EXPOSE 3306
```

**Zugriff vom Host erlauben** <br>
Um via Host auf den Container zuzugreifen sind ein paar Arbeiten zu erledigen.

Installation des MySQL Clients auf dem Host:
```Shell
    $ sudo apt-get install mysql-client
```

Freigabe des Ports in der MySQL-Config im Container, z.B. via Dockerfile:
```Shell
    RUN sed -i -e"s/^bind-address\s*=\s*127.0.0.1/bind-address = 0.0.0.0/" /etc/mysql/my.cnf
```
>>>
:warning: **Sicherheitsauswirkung**

Das Ändern der Bind-Adresse des MySQL-Servers von 127.0.0.1 (nur localhost) auf 0.0.0.0 (alle Netzwerk-Interfaces) wird der MySQL-Server aus dem gesamten Netzwerk erreichbar, nicht nur vom lokalen Host!
>>>

SQL Freigabe, via MySQL Client im Container einrichten:
```SQL
    CREATE USER 'root'@'%' IDENTIFIED BY 'admin';
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'%';
    FLUSH PRIVILEGES;
```

Sind alle Arbeiten durchgeführt, sollte mit folgenden Befehl vom Host auf den MySQL Server, im Docker Container, zugegriffen werden können:
```Shell
    $ mysql -u root -p admin -h 127.0.0.1
```


### Container-Networking
***
Bei Docker können "Netzwerke" getrennt von Containern erstellt und verwaltet werden.

Wenn man Container startet, lassen sie sich einem bestehenden Netzwerk zuweisen, sodass sie sich direkt mit anderen Containern im gleichen Netzwerk austauschen können.

Standardmässig werden folgende Netzwerke eingerichtet:
* bridge 
    * Das Standard-Netzwerk indem gemappte Ports gegen aussen sichtbar sind.
* none
    * Für Container ohne Netzwerkschnittstelle bzw. ohne Netzanbindung.
* host
    * Fügt den Containern dem internen Host-Netzwerk hinzu, Ports sind nicht nach aussen sichtbar.

**Befehle** <br>

Auflisten der bestehenden Netzwerke:
```Shell
    $ docker network ls
```

Detailinformationen, inkl. der Laufenden Container zu einem Netzwerk:
```Shell
    $ docker network inspect bridge
```

Container erstellen ohne Netzwerkschnittstelle:
```Shell
    $ docker run --network=none -it --name c1 --rm busybox
    
    # Kontrollieren im Container 
    $ ifconfig  
```

Container erstellen mit dem Host-Netzwerk:
```Shell
    $ docker run --network=host -itd --name c1 --rm busybox
    
    # Kontrollieren mittels
    $ docker inspect host
```

Erstellen eines neuen Brigde-Netzwerk:
```Shell
    $ docker network create --driver bridge isolated_nw
```

Vorheriges MySQL- & Ubuntu-Beispiel starten und mit neuem Bridge-Netzwerk verbinden:
```Shell
    $ docker run --rm -d --network=isolated_nw --name mysql mysql
    $ docker run -it --rm --network=isolated_nw --name ubuntu ubuntu:14.04 bash
```

Im Ubuntu Container, Verbindung zum MySQL Server Port überprüfen:
```Shell
    $ sudo apt-get update && sudo apt-get install -y curl
    $ curl -f http://mysql:3306
```
>>>
:x: **Container-Linking (veraltet)** <br>
Docker-Links sind die einfachste Möglichkeit, Container auf dem gleichen Host miteinander reden lassen zu können. Nutzt man das Standardnetzwerk-Modell von Docker, geschieht die Kommunikation zwischen Containern über ein internes Docker-Netzwerk, so dass sie nicht im Host-Netzwerk erreichbar sind.

Beispiel:
```Shell
    $ docker run -it --rm --link mysql:mysql ubuntu:14.04 bash
```

Innerhalb des MySQL-Containers:
```Shell
    env

    PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
    HOSTNAME=53a8e2acc32c
    MYSQL_PORT=tcp://172.17.0.2:3306
    MYSQL_PORT_3306_TCP=tcp://172.17.0.2:3306
    MYSQL_PORT_3306_TCP_ADDR=172.17.0.2
    MYSQL_PORT_3306_TCP_PORT=3306
    MYSQL_PORT_3306_TCP_PROTO=tcp
    MYSQL_NAME=/tender_feynman/mysql
    HOME=/root

    sudo apt-get update && sudo apt-get install -y curl mysql-client    
```

Testen ob der Port aktiv ist (auf Ubuntu):
```Shell
    $ curl -f http://${MYSQL_PORT_3306_TCP_ADDR}:${MYSQL_PORT_3306_TCP_PORT}
```

MySQL Client starten (auf Ubuntu):
```Shell
    $  mysql -u root -p admin -h ${MYSQL_PORT_3306_TCP_ADDR}
```
>>>

# ![](../images/Volume_36x36.png?raw=true "Volumes") 04 - Volumes

> [⇧ **Nach oben**](#m169---30-container)

Bis jetzt gingen alle Änderungen im Dateisystem beim Löschen des Docker Containers komplett verloren.

Um Daten persistent zu halten, stellt Docker verschiedene Möglichkeiten zur Verfügung:
* Ablegen der Daten auf dem Host
* Sharen der Daten zwischen Container
* Eigene, sogenannte Volumes erstellen, zum Ablegen von Daten

**Erweiterung im Dockerfile** <br>
Um Daten auf dem Host oder in Volumes zu speichern, sind die Verzeichnis mit den Daten via `VOLUME` im Dockerfile einzutragen.

Beispiel MySQL:
```Shell
    VOLUME /var/lib/mysql
```


### Volumes
***
Volumes sind ein spezielles Verzeichnis auf dem Host in dem einer oder mehrere Container ihre Daten ablegen.

Volumes bieten mehrere nützliche Funktionen für persistente oder gemeinsam genutzte Daten:
* Volumes werden initialisiert, wenn ein Container erstellt wird.
* Wenn das Image des Containers, Daten am angegebenen Einhängepunkt enthält, werden die vorhandenen Daten nach der Volumeninitialisierung in das neue Volume kopiert.
* Volumes können gemeinsam genutzt und unter Containern wiederverwendet werden.
* Änderungen an einem "Data volumes" erfolgen direkt.
* Änderungen an einem "Data volumes" werden nicht berücksichtigt, wenn Sie ein Image aktualisieren.
* Volumes bleiben bestehen, auch wenn der Container selbst gelöscht wird.
* Volumes sind so ausgelegt, dass die Daten unabhängig vom Lebenszyklus des Containers bestehen bleiben.
* Docker löscht nie automatisch Volumes, wenn Sie einen Container entfernen, kann deshalb "Müll" übrigbleiben

<details><summary>Ausführliche Erklärung zu Container-Volumes</summary>

***In der Welt der Docker-Containerisierung spielen Volumes eine entscheidende Rolle bei der Datenspeicherung und -persistenz. Ein Docker Volume ist ein spezielles Verzeichnis auf dem Host-System, das einem oder mehreren Containern zugewiesen werden kann, um Daten persistent zu speichern. Diese Volumes bieten eine flexible und effiziente Lösung für die Verwaltung von Daten, die über den Lebenszyklus einzelner Container hinausgehen sollen.***

**Die Kernfunktionalitäten von Docker Volumes umfassen:**

 - **Initialisierung bei Containererstellung:** Ein Volume wird automatisch erstellt und initialisiert, sobald ein neuer Container mit einem entsprechenden Volumen-Konfigurationsbefehl gestartet wird.
 - **Datenkopie bei Volumeninitialisierung:** Existieren bereits Daten am spezifizierten Mountpunkt eines Container-Images, werden diese in das neu erstellte Volume kopiert. Dies ermöglicht eine nahtlose Datenmigration und -initialisierung.
 - **Wiederverwendbarkeit und gemeinsame Nutzung:** Volumes können von verschiedenen Containern gemeinsam genutzt werden, was die Datenverwaltung in komplexen Anwendungen mit mehreren Diensten vereinfacht.
- **Direkte Änderungen:** Modifikationen in Data Volumes werden in Echtzeit reflektiert, ohne die Notwendigkeit, den Container neu zu starten oder das Image zu aktualisieren.
 - **Persistenz über Containerlebenszyklus:** Volumes sind unabhängig von der Lebensdauer der Container, die sie nutzen. Selbst nach dem Löschen eines Containers bleiben die in Volumes gespeicherten Daten erhalten.
 - **Unabhängigkeit vom Containerlebenszyklus:** Die in Volumes gespeicherten Daten bleiben bestehen, selbst wenn der Container gelöscht wird, was eine dauerhafte Datenspeicherung gewährleistet.
 - **Keine automatische Löschung durch Docker:** Docker führt keine automatische Löschung von Volumes durch, wenn ein Container entfernt wird. Dies verhindert Datenverlust, kann jedoch zu nicht mehr benötigten Daten ("Müll") führen, die manuell verwaltet werden müssen.

Durch die Nutzung von Docker Volumes können Entwickler und Administratoren effektiv sicherstellen, dass wichtige Daten auch nach dem Löschen oder Neustarten von Containern erhalten bleiben. Dies bietet eine robuste Grundlage für persistente Datenspeicherung und -zugriff in containerisierten Anwendungen.
</p></details>

**Beispiele** <br>
Busybox Container starten und neues Volume `/data` anlegen:
```Shell
    $ docker run --network=host -it --name c2 -v /data --rm busybox
    
    # Im Container
    $ cd /data
    $ mkdir t1
    $ echo "Test" >t1/test.txt
    
    # CTRL + P + CTRL + Q
    $ docker inspect c2
    
    # Nach Mounts suchen, z.B. 
            "Mounts": [
            {
                "Type": "volume",
                "Name": "ea99634523a0aa3d6fbf7ee02c491029170d7105f9c5404760a71e046ad55c67",
                "Source": "/var/lib/docker/volumes/ea99634523a0aa3d6fbf7ee02c491029170d7105f9c5404760a71e046ad55c67/_data",
                "Destination": "/data",
                "Driver": "local",
                "Mode": "",
                "RW": true,
                "Propagation": ""
            }
    
    # Datei ausgeben (auf Host)
    $ sudo cat /var/lib/docker/volumes/ea99634523a0aa3d6fbf7ee02c491029170d7105f9c5404760a71e046ad55c67/_data/t1/test.txt
```

Datenverzeichnis `/var/lib/mysql` vom Container auf dem Host einhängen (mount):
```Shell
    $ docker run -d -p 3306:3306  -v ~/data/mysql:/var/lib/mysql --name mysql --rm mysql
    
    # Datenverzeichnis
    $ ls -l ~/data/mysql
```

Einzelne Datei auf dem Host einhängen:
```Shell
    $ docker run --rm -it -v ~/.bash_history:/root/.bash_history ubuntu /bin/bash
```


### Datencontainer
***
Früher wurden Datencontainer erstellt, deren einziger Zweck das gemeinsame Nutzen von Daten durch andere Container war.

Dazu musste zuerst ein Container via `docker run` gestartet werden, damit andere via `--volumes-from` darauf zugreifen konnten.

Diese Methode war zwar funktionsfähig aber nicht ausbaufähig.
 
**Beispiel** <br>
Container mit Datencontainer `dbdata` erstellen:
```Shell
    $ docker create -v /dbdata --name dbstore busybox 
```

Zweiten Container erstellen, welcher auf den Datencontainer `dbdata` zugreift:
```Shell
    $  docker run -it --volumes-from dbstore --name db busybox
    
    # Im Container
    $ ls -l /dbdata
```

Der Datencontainer `dbdata` ist nun unter dem root-Verzeichnis als `/dbdata` eingehängt.

<details><summary>Best Practices für die Verwendung von Docker Volumes</summary>

1. **Klare Trennung von persistenten und ephemeren Daten**: Speichern Sie nur Daten, die persistent bleiben müssen (z.B. Datenbankdateien, wichtige Logs, Konfigurationsdateien), in Volumes. Vermeiden Sie die Speicherung von ephemeren Daten, die leicht wiederherstellbar sind oder deren Verlust keinen signifikanten Einfluss hat.

2. **Verwendung von Named Volumes**: Benennen Sie Ihre Volumes klar und eindeutig. Named Volumes erleichtern die Verwaltung, Backup und Wiederherstellung von Daten. Beispiel: Verwenden Sie `docker volume create myapp-data` anstatt anonyme Volumes.

3. **Sicherheitskopien regelmässig erstellen**: Automatisieren Sie Backups Ihrer Volumes, um Datenverluste zu vermeiden. Tools wie `rsync` oder spezifische Docker-Backup-Tools können hierfür eingesetzt werden.

4. **Zugriffsrechte sorgfältig verwalten**: Achten Sie darauf, dass die Container nur die minimal notwendigen Rechte haben, um auf die Volumes zuzugreifen. Verwenden Sie nach Möglichkeit nicht-root Benutzer in Ihren Containern.

**Spezifische Empfehlungen**

1. **Multi-Container Zugriffe koordinieren**: Wenn mehrere Container auf ein Volume zugreifen müssen, stellen Sie sicher, dass Zugriffskonflikte vermieden werden. Verwenden Sie beispielsweise File-Locking-Mechanismen oder spezifische Datenbank-Tools, die parallele Zugriffe verwalten können.

2. **Verwendung von Volume Containern**: Für komplexere Anwendungen, die Daten zwischen Containern teilen müssen, erwägen Sie die Verwendung eines dedizierten "Daten-Containers". Dieser Ansatz ist zwar mit der Einführung von Named Volumes weniger verbreitet, kann aber in bestimmten Szenarien noch sinnvoll sein.

3. **Performance Optimierungen**: Beachten Sie, dass der Zugriff auf Daten in Volumes je nach Speicherort (lokal, NAS, Cloud-Storage) Latenzen verursachen kann. Optimieren Sie die Performance durch geeignete Wahl des Speicherortes und Caching-Strategien.

4. **Replikation und Skalierbarkeit**: Für Produktionsumgebungen, die hohe Verfügbarkeit erfordern, planen Sie die Replikation Ihrer Volumes. Docker-Swarm und Kubernetes bieten Mechanismen zur Verwaltung von Volumes in skalierten und verteilten Anwendungen.

**Technische Umsetzung**

 - **Docker Compose für lokale Entwicklung**: Nutzen Sie Docker Compose, um Volumes in Ihrer lokalen Entwicklungsumgebung zu definieren und zu verwalten. Beispiel:

 ```
 services:
  db:
    image: postgres
    volumes:
      - db-data:/var/lib/postgresql/data
 volumes:
  db-data:
 ```
</p></details>

### Named Volumes
***
Seit der Version 1.9 von Docker existiert das Kommando `docker volume` zur Verwaltung von Volumes auf einem Docker Host:
 * Man kann damit verschiedene Volume-Driver-Dateisysteme für Container bereitstellen.
 * Ein Volume kann nun auf einem Host angelegt werden und dem verschiedenen Container bereitgestellt werden.
 * Volumes können einheitlich mit diesen Befehlen verwaltet werden.
 * Wenn keine Default-Dateien auf dem Volume benötigt werden, kann auf einen separaten Datencontainer verzichtet werden.
 * Mit diesem Schritt können nun verschiedene Dateisysteme und Optionen effizient in Containern genutzt werden.

**Beispiele** <br>
Erstelle eine Volume `mysql`:
```Shell
    $ docker volume create mysql
```

Ausgabe aller vorhandenen Volumes:
```Shell
    $ docker volume ls
```

Erstellt einen Container `c2` und hängt das Volume unter `/var/lib/mysql` ein:
```Shell
    $ docker run  -it --name c2 -v mysql:/var/lib/mysql --rm busybox
```

Die Abhängigkeit Volume Verzeichnis kann auch im Dockerfile hinterlegt werden:
```Shell
    VOLUME mysql:/var/lib/mysql
```


# ![](../images/Share_36x36.png "Image-Bereitstellung") 05 - Image-Bereitstellung

> [⇧ **Nach oben**](#m169---30-container)

Hat man eigene Images erstellt, werden kann man sie auch für andere bereitstellen – sei es für Kollegen, auf Continuous-Integration-Servern oder für Endanwender.

Es gibt viele verschiedene Möglichkeiten, Images bereitzustellen: Man kann sie aus Dockerfiles neu bauen, aus einer Registry `docker pull` ziehen oder sie mit `docker load` aus einer Archivdatei installieren.

**Namensgebung für Images** <br>
Images bestehen aus Namen und Version (TAG), z.B. ubuntu:16.04. Wird keine Version Angegeben wird automatisch :latest angefügt.

Beim Bereitstellen von Images ist es sehr wichtig, beschreibende und exakte Namen und Tags einzusetzen.

Imagenamen und -Tags werden beim Bauen der Images oder durch den Befehl `docker tag` gesetzt:
```Shell
    $ docker build -t mysql .
    $ docker build -t mysql:1.0 .
    $ docker tag mysql username/mysql 
```

Die Tag-Namen müssen sich an ein paar Regeln halten. Sie müssen aus Gross- und Kleinbuchstaben, Zahlen oder den Symbolen . und - bestehen. Sie dürfen nicht länger als 128 Zeichen sein. Beim ersten Zeichen darf es sich nicht um ein . oder - handeln.

Namen von Repositories und Tags sind ausgesprochen wichtig, wenn man einen Entwicklungs-Workflow aufbauen möchte. Docker hat nur sehr wenige Regeln für gültige Namen und erlaubt es jederzeit, Namen zu erstellen oder zu löschen. Es liegt also am Entwicklungsteam, ein vernünftiges Namensschema zu entwerfen und umzusetzen.

**Warnung vor dem latest-Tag** <br>
Docker nutzt `latest` als Standardwert, wenn kein Tag vergeben wurde, darüber hinaus hat es aber keine spezielle Bedeutung. Viele Repositories verwenden es als Alias für das aktuellste stabile Image, dabei handelt es sich aber nur um eine Konvention, die durch keinerlei technische Massnahmen erzwungen wird.

Bezieht sich ein `docker run` oder `docker pull` auf einen Imagenamen ohne Tag, wird Docker das Image verwenden, das mit latest gekennzeichnet ist. Gibt es kein solches Image, wird ein Fehler ausgegeben.


### Docker Hub
***
Die einfachste Möglichkeit, eigene Images bereitzustellen, ist der Einsatz des Dockers Hub.

Bei diesem handelt es sich um die von Docker Inc. angebotene Online-Registry.

Der Hub ermöglicht kostenlose Repositories für öffentliche Images, die Anwender können aber auch für private Repositories bezahlen.

**Docker Hub einrichten** <br>
Um seine eigenen Images auf Docker Hub hochzuladen ist wie folgt vorzugehen: 
1. Acount auf Docker Hub eröffnen.
2. Imagenamen mit Usernamen, laut Account auf Docker Hub, taggen:
    ```Shell
        $ docker tag mysql username/mysql
    ```
3. Image hochladen:
    ```Shell
        $ docker push username/mysql
    ```
4. Dashboard auf Docker Hub anwählen und Image beschreiben.

**Weitere Befehle** <br>

Suchen nach Images auf Docker Hub:
```Shell
    $ docker search mysql
```

Image herunterladen, z.B. um Build-Zeiten zu vermindern:
```Shell
    $ docker pull ubuntu
```


### Export/Import von Container und Images 
***
Um Container und Images einfach nur zwischen verschiedenen Hosts hin und her zu verschieben, wird keine Registry benötigt.

Container können mittels `docker export` und `docker import` und Images mittels `docker save` und `docker load` von/nach Verzeichnisse kopiert werden.

**Container** <br>

Container exportieren:
```Shell
    $ docker ps

        CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
        7fd371d71357        vagrant_apache      "/bin/sh -c '/bin/..."   3 hours ago         Up 3 hours          0.0.0.0:8080->80/tcp   vagrant_apache_1

    $ docker export vagrant_apache_1 -o va1.tar

    $ ls -lh

        total 200M
        -rwxrwxrwx 1 ubuntu ubuntu  731 Feb  2 08:28 Dockerfile
        -rwxrwxrwx 1 ubuntu ubuntu 200M Feb  2 12:36 va1.tar
```

Container importieren, z.B. auf einem anderen Host (dabei wir ein Image erzeugt):
```Shell
    $ docker import va1.tar va1

    $ docker images

        REPOSITORY          TAG                 IMAGE ID            CREATED                  SIZE
        va1                 latest              167ec5ca640c        Less than a second ago   200 MB
```

**Images** <br>

Eigene Images ausgeben:
```Shell
    /vagrant/mysql$ docker images

        REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
        mysql               latest              24be8efe0428        2 hours ago         346 MB
        apache              latest              4221b4f12ce8        2 hours ago         225 MB

```

Images im TAR-Format mit `save` sichern:
```Shell
    /vagrant/mysql$ docker save mysql -o mysql.tar
    /vagrant/mysql$ docker save apache -o apache.tar
```

Images mit `load` wiederherstellen:
```Shell
    $ docker load -i mysql.tar  
```

### Private Registry 
***
Es gibt neben dem Docker Hub noch ein paar weitere Möglichkeiten.

Mann könnte alles manuell machen, indem Images exportiert und wieder importiert oder einfach auf jedem Docker Host aus Dockerfiles neue Images gebaut werden.

Beide Lösungen sind suboptimal: <br>
Das immer neue Bauen aus Dockerfiles ist langsam und kann auf den verschiedenen Hosts zu unterschiedlichen Images führen, während das Exportieren und Importieren von Images knifflig und fehleranfällig sein kann.

Die verbleibende Möglichkeit ist, eine andere Registry zu verwenden, die entweder von einem selbst oder durch eine andere Firma gehostet werden kann.

**Private Docker Registry einrichten** <br>
```Shell
    $ sudo docker pull registry:2
    
    $ sudo docker run -d -p 5000:5000 --restart=always --name registry \ 
    -v /var/spool/docker-registry:/var/lib/registry registry:2
```

**Docker Client auf Registry zusteuern** <br>
Die Docker Clients steuern per default auf Docker Hub zu. Damit sie mit der lokalen Registry arbeiten kann, ist die Datei `/etc/docker/daemon.json` mit folgendem Inhalt zu erstellen und Docker neu zu starten (`sudo docker restart`):

```Shell
    { "insecure-registries":["{{config.docker}}:5000"] }
```

Anschliessend können die vorhanden Images von unserer lokalen Docker Registry geholt werden (pull):
```Shell
    $ docker pull {{config.docker}}:5000/ubuntu
```

(...) oder geschrieben werden (push):
```Shell
    $ docker tag ubuntu {{config.docker}}:5000/myubuntu
    $ docker push {{config.docker}}:5000/myubuntu
```

**Wichtig:** `{{config.docker}}` durch installierten Server ersetzen.

<br>

> [⇧ **Nach oben**](#m169---30-container)

<br>


# ![](../images/podman36x36.png?raw=true "Podman") 06 - Podman

### YouTube Einführung
[![](../images/podman_titel.png)](https://www.youtube.com/watch?v=0jhdCcAc8nM)

Ab. **1:31** beginnen (Werbung überspringen) - Gesamtdauer ca. **10Min.**

### Einstieg (Podman vs. Docker)
**Podman** und **Docker** sind beides **Container-Laufzeitumgebungen**, die es Entwicklern ermöglichen, Anwendungen in isolierten und portablen Containern auszuführen. Es gibt jedoch **einige wichtige Unterschiede** zwischen den beiden, die ich im Folgenden zusammenfassen werde:

- **Architektur:** Ein großer Unterschied zwischen Podman und Docker ist ihre Architektur. Podman ist eine daemonlose Container-Laufzeitumgebung, während Docker auf einem zentralen Daemon-Service-Modell basiert. Das bedeutet, dass Podman ohne einen Hintergrund-Daemon läuft und Container als eigenständige Prozesse startet, was eine erhöhte Sicherheit bietet und die Abhängigkeit von einem zentralen Daemon verringert.
- **Sicherheit:** Podman wurde von Grund auf mit Sicherheit im Hinterkopf entwickelt. Da Podman ohne einen Hintergrund-Daemon läuft, gibt es weniger Angriffsvektoren für potenzielle Sicherheitsbedrohungen. Podman verwendet auch standardmäßig Rootless-Container, was bedeutet, dass Container als nicht-root-Benutzer ausgeführt werden, um die Sicherheit weiter zu erhöhen.
- **Kompatibilität:** Podman ist kompatibel mit der Docker-Container-Laufzeitumgebung, was bedeutet, dass Sie Docker-Images und -Container mit Podman verwenden können. Podman bietet auch eine Docker-API-Emulation, die Entwicklern ermöglicht, bestehende Docker-Tools und -Befehle ohne Änderungen zu verwenden.
- **Lizenz:** Ein weiterer Unterschied zwischen Podman und Docker ist ihre Lizenz. Podman ist unter der Apache 2.0-Lizenz lizenziert, während Docker unter der GPLv2-Lizenz lizenziert ist. Dies hat einige Auswirkungen auf die Art und Weise, wie Sie Podman und Docker verwenden und anpassen können.

Zusammenfassend lässt sich sagen, dass Podman eine **sichere, daemonlose Container-Laufzeitumgebung** ist, die mit Docker **kompatibel** ist und eine breite Palette von Entwicklungs- und Bereitstellungsszenarien unterstützt. Wenn Sie auf der Suche nach einer Container-Laufzeitumgebung sind, die Sicherheit und Kompatibilität mit Docker bietet, könnte Podman eine gute Wahl sein.


### Daemonless

Docker steht breits seit längerem im Fokus von bzgl. Sicherheit, da der Docker Daemon, also der Dienst, der für die Verwaltung der Container (starten, stoppen, etc.) zuständig ist, als **Root-Benutzer** ausgeführt wird. Dies ist notwending, weil Docker noch weitere Dienste zur Verfügung stellt, wie beispielsweise Overlay-Netzwerke im **Docker-Swarm**-Betrieb. Podman arbeitet grundsätzlich **ohne** einen zentralen Daemon und stellt somit ein Werkzeug dar, das **im User-Space** und damit im Kontext der **jeweiligen Benutzerrechte** ausgeführt werden kann.

### Rootless

Rootless-Container bedeuten, dass es möglich ist, einen Container **ohne Root-Berechtigungen** zu starten. Dank des Mappings der Benutzer-IDs in den Namespace des laufenden Containers besitzt der Container **keine Root-Rechte**. Wie in der nächsten Abbildung dargestellt, wird zuerst mittels Podman ein Alpine-Container gestartet, welcher wiederum einen sleep-Befehl startet. Nachdem der Container gestartet ist, kann im Host-Betriebssystem überprüft werden, mit welchem Benutzer der sleep-Befehl darin ausgeführt wird. Im gezeigten Beispiel ist dies **ubuntu**, mein eigener Linux-Benutzer ohne Root-Rechte. Der zweite Befehl, welcher innerhalb des laufenden Containers ausgeführt wird, zeigt, dass innerhalb des Containers der sleep-Befehl mit Root-Rechten gestartet wurde. Das Mapping der Benutzer-IDs funktioniert also ausgezeichnet – innerhalb des Containers läuft ein Prozess mit Root-Rechten, welcher jedoch **innerhalb des Host-Betriebssystems** nur **normale Benutzer-Berechtigungen** besitzt.

![Console](../images/podman_com_800.png "Podman")

Diese Funktionalität stellt gegenüber der Vorgehensweise von Docker einen wesentlichen Fortschritt dar. Docker unterstützt die Ausführung des Docker Daemons in der rootless-Variante experimentell erst ab Version 19.03. Die Unterstützung von rootless-Containern steht hier jedoch erst am Anfang der Implementierung, da diese Veränderung innerhalb der Docker-Basis-Technologie viele Bereiche betrifft, unter anderem den Netzwerkbereich mit den Overlay-Netzwerken im Docker-Swarm-Betrieb.

Beim Betrieb von rootless-Containern gilt es ebenso zu beachten, dass **keine Low Ports** für das Host-Port-Mapping verwendet werden dürfen. Für den Start eines Containers (oder Pods) mit manuellem Port Expose bedeutet dies, dass ein **Port höher als 1024** verwendet werden muss. Wird dieser Umstand nicht beachtet, so bricht der Podman-Befehl mit einer eher kryptischen Fehlermeldung, wie im nächsten Screenshot festgehalten ist, ab.

![Lowport](../images/podman_port_800.png)

### Fazit

Podman bietet mit den daemonless- und rootless-Möglichkeiten viel Potential, Container **sicher** betreiben zu können. Insbesondere im **Kubernetes-Umfeld**, bspw. bei Red Hats OpenShift, hat sich Podman bereits als Standard durchgesetzt. Die Aufteilung in mehrere verschiedene kleinere Dienste, podman, conmon und slirp4netns entspricht dabei der Unix-Philosophie: „Make each program do one thing well“. 

### Befehle

Podman ist kompatibel mit der Docker-API, was bedeutet, dass viele Docker-Befehle direkt in Podman ausgeführt werden können. Hier sind einige Beispiele für die Verwendung von Podman, um Docker-Befehle auszuführen:

**Docker-Image herunterladen:**
```Shell
    $ podman pull <image-name>
    $ podman pull docker.io/nginx
```

**Startet einen Container mit einer interaktiven Shell**  (interactive, tty):
```Shell
    $ podman run -it <image-name> /bin/bash
    $ podman run -it ubuntu /bin/bash
```

**Docker-Container stoppen:**
```Shell
    $ podman stop <container-id>
    $ podman stop 536075344d
```

**Docker-Container löschen:**
```Shell
    $ podman rm <container-id>
    $ podman rm 536075344d
```

Es gibt viele weitere Docker-Befehle, die in Podman ausgeführt werden können, einschließlich des Erstellens von Images, der Verwendung von Volumes und Netzwerken, der Ausführung von Befehlen innerhalb von Containern und vieles mehr. Wenn Sie mehr über die Verwendung von Podman erfahren möchten, empfehle ich Ihnen, die offizielle Dokumentation zu lesen.

**Links**

- [Podman Commands](https://docs.podman.io/en/latest/Commands.html) - Offizielle **Doku-Webseite** von Podman.io
- [Podman Tutorials](https://docs.podman.io/en/latest/Tutorials.html) - Offizielle **Tutorials-Webseite** von Podman.io





# ![](../images/Reflexion_36x36.png "Fazit / Reflexion") 07 - Reflexion

> [⇧ **Nach oben**](#m169---30-container)


 # ![](../images/Samples_36x36.png "LB2") 08 - LB2

### Inhalte
- [Arbeitsauftrag](M169_LB2_IaC_v2.pdf)
- [Bewertungsraster](M169_LB2_Bewertung.png)
- [Technische Details, Anforderungen und Lösungsansätze](LB2.md)

 # ![](../images/Samples_36x36.png "Docker") 09 - Beispiele (für LB2)

> [⇧ **Nach oben**](#m169---30-container)

1.  Terminal (*Bash*) öffnen
2.  [VM (beinhaltet Docker)](../docker/) erstellen und darin wechseln

```Shell
    cd M169/docker
    vagrant up
    vagrant ssh
```

3.  In der VM ins Verzeichnis `/vagrant<Beispiel>` wechseln und `README.md` studieren, z.B. mittels `less README.md`.

Es stehen folgende Beispiele zur Verfügung:

* [apache - Apache Web Server](../docker/apache/)
* [mysql - MySQL Datenbank](../docker/mysql/)
* [apache4X - Scriptscript welches 4 Web Server Container erstellt](../docker/apache4X/)
* [compose - Docker Compose](../docker/compose/)
* [dotnet - .NET Entwicklungsumgebung](d../docker/otnet/)
* [microservice - Micro Service mit Node.js](../docker/microservice/)
* [jenkins - Build (CI/CD) Umgebung](../docker/jenkins/)


# ![](../images/Magnifier_36x36.png "Quellenverzeichnis") 10 - Quellenverzeichnis
 * [CNF-Cloud Native Computing Foundation - Blog](https://www.cncf.io/blog/)
 * [docker docs](https://docs.docker.com/)
 * [dockerhub - The Apache HTTP Server Project](https://hub.docker.com/_/httpd)
 * [How to Use the Apache httpd Docker Official Image](https://www.docker.com/blog/how-to-use-the-apache-httpd-docker-official-image/)
 * [How to Setup Apache Web Server in a Docker Containe](https://www.tecmint.com/install-apache-web-server-in-a-docker-container/)
 * [Setting up and Running a MySQL Container](https://www.baeldung.com/ops/docker-mysql-container)
 * [What is Jenkins? The CI server explained](https://www.infoworld.com/article/3239666/what-is-jenkins-the-ci-server-explained.html)
 * [How to Setup Jenkins in Docker Container on AWS EC2 instance](https://www.geeksforgeeks.org/how-to-setup-jenkins-in-docker-container/)

## Glosar
### Allgemeine Begriffe
- **Container**: Leichtgewichtige, ausführbare Softwarepakete, die Code, Laufzeitumgebung, Systemtools, Bibliotheken und Einstellungen enthalten.
- **Docker**: Eine Open-Source-Plattform zur Automatisierung des Deployments von Anwendungen als portable, selbst-suffiziente Container, die auf jeder Linux-Distribution oder Windows-System laufen können.
- **Microservices**: Ein Architekturstil, der eine Anwendung als Sammlung kleiner, unabhängiger Dienste entwickelt, die über wohldefinierte APIs kommunizieren.
- **Persistenz**: In der Docker-Containerisierung bezieht sich Persistenz auf die Fähigkeit, Daten über den Lebenszyklus eines Containers hinaus dauerhaft zu speichern. Dies wird durch Volumes erreicht, spezielle Bereiche auf dem Host, die Daten unabhängig vom Zustand des Containers erhalten.
- **Volume**: Ein Mechanismus in Docker, der es ermöglicht, Daten persistent zu speichern und zwischen Container zu teilen.

### Spezifische Begriffe
- **Dockerfile**: Eine Textdatei mit Anweisungen zum automatischen Erstellen eines Docker-Images.
- **Containerlebenszyklus**: Umfasst die Phasen der Erstellung, des Starts, der Ausführung und des Löschens eines Docker-Containers. Die Konfiguration wird durch das Dockerfile und Startparameter definiert, während Volumes die Datenpersistenz über den Zyklus hinaus sichern.
- **Image**: Ein Docker-Image ist eine unveränderliche Datei, die eine Momentaufnahme einer Anwendung und ihrer Umgebung beinhaltet. Es dient als Vorlage für die Erstellung von Docker-Containern und enthält alle notwendigen Komponenten wie den Anwendungscode, Laufzeitumgebung, Bibliotheken und Abhängigkeiten. Images ermöglichen die Konsistenz und Portabilität von Anwendungen, indem sie gewährleisten, dass eine Anwendung über verschiedene Umgebungen hinweg identisch funktioniert.
- **Registry**: Ein Dienst zur Speicherung und Verteilung von Docker-Images.
- **Docker Daemon**: Der Hintergrunddienst, der auf dem Hostsystem läuft und die Docker-Container verwaltet.
- **Docker Client**: Das Kommandozeilen-Tool, das es ermöglicht, mit dem Docker Daemon zu kommunizieren.
- **Docker Hub**: Eine Cloud-basierte Registry, die es ermöglicht, Docker-Images zu teilen, zu speichern und zu verwalten.
- **Build Context**: Der Kontext, in dem Docker das Image baut. Es bezieht sich auf die Dateien und Verzeichnisse, die im Dockerfile durch die ADD- oder COPY-Anweisungen referenziert sind.
- **Layer / Imageschichten**: Jede Anweisung in einem Dockerfile erstellt eine neue Schicht im Docker-Image, was das Image wiederverwendbar und die Verteilung effizienter macht.

### Befehle
- **docker build**: Baut ein Docker-Image aus einem Dockerfile.
- **docker pull**: Lädt ein Docker-Image aus einer Registry herunter.
- **docker run**: Erstellt und startet einen Container aus einem Image.
- **docker push**: Lädt ein lokal gebautes Image in eine Registry hoch.
- **docker rm**: Löscht einen oder mehrere Container.
- **docker rmi**: Löscht eines oder mehrere Docker-Images.
- **docker volume create**: Erstellt ein neues Volume.
- **docker volume ls**: Listet alle Volumes auf.
- **docker inspect**: Gibt detaillierte Informationen über Container oder Images aus.

<br>


---

> [:arrow_up: **Nach oben**](#m169---30-container)

---

> [:arrow_up: **Zurück zur Hauptseite**](../README.md)

___
