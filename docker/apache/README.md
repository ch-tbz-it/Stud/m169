Ubuntu/Docker mit Apache
------------------------

### Übersicht 

    +---------------------------------------------------------------+
    !                                                               !	
    !    +-------------------------+                                !
    !    ! Web-Server              !                                !       
    !    ! Port: 80                !                                !       
    !    ! Volume: /var/www/html   !                                !       
    !    +-------------------------+                                !
    !                                                               !	
    ! Container                                                     !	
    +---------------------------------------------------------------+
    ! Container-Engine: Docker                                      !	
    +---------------------------------------------------------------+
    ! Gast OS: Ubuntu 16.04                                         !	
    +---------------------------------------------------------------+
    ! Hypervisor: VirtualBox                                        !	
    +---------------------------------------------------------------+
    ! Host-OS: Windows, MacOS, Linux                                !	
    +---------------------------------------------------------------+
    ! Notebook - Schulnetz 10.x.x.x                                 !                 
    +---------------------------------------------------------------+

Übersicht als Plantuml
```plantuml
 @startuml

component "1. Notebook - Schulnetz 10.x.x.x"  { 
  component "2. Host-OS: Windows, MacOS, Linux"  {
    component "3. Hypervisor: VirtualBox"  {
      component "4. Gast OS: Ubuntu 16.04" {
        component "5. Container-Engine: Docker" {
          component "6. Container: Web-Server" {
             package "Port:" <<80>> 
             package "Volume:" <</var/www/html>> 
          }
        }
      }
    }
  }
}

"1. Notebook - Schulnetz 10.x.x.x" <--> "2. Host-OS: Windows, MacOS, Linux"

"2. Host-OS: Windows, MacOS, Linux" <--> "3. Hypervisor: VirtualBox"

"3. Hypervisor: VirtualBox" <--> "4. Gast OS: Ubuntu 16.04"

"4. Gast OS: Ubuntu 16.04" <--> "5. Container-Engine: Docker"

"5. Container-Engine: Docker" <--> "6. Container: Web-Server"

@enduml

```
	
### Beschreibung

Ubuntu/Docker mit Apache Container und Daten (index.html) im HOME Verzeichnis unter web.

Docker Container builden:

	cd /vagrant/apache
	docker build -t apache .

Docker Container starten:

	docker run --rm -d -p 8080:80 -v `pwd`/web:/var/www/html --name apache apache

Funktionsfähigkeit überprüfen Host -> Docker Container

	curl http://localhost:8080
	
Es muss der Inhalt von web/index.html angezeigt werden.

**Testen**

`web/index.html` Datei editieren und mittels `curl` oder Browser testen.


