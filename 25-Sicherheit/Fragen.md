# Fragen

## Firewall und Reverse Proxy

:question: Was ist der Unterschied zwischen einem Web Server und einen Reverse Proxy?
<details><summary>:white_check_mark: Antwort </summary><p>

 Web Server handelt HTML Seiten direkt ab, Reverse Proxy dient als Stellvertretter für einen Web Server o.ä.	
</p></details>

---

:question: Was verstehen wir unter einer "White List"?
<details><summary>:white_check_mark: Antwort</summary><p>

 Eine White List, fasst im Gegensatz zu einer Black List, Vertrauenswürdige Elemente, z.B. Server zusammen.
</p></details>
	
---

:question: Was wäre die Alternative zum Absichern der einzelnen Server mit einer Firewall?
<details><summary>:white_check_mark: Antwort</summary><p>

 Eine Zentrale Firewall
</p></details>

---

### ssh
***

:question: Was ist der Unterschied zwischen der id_rsa und id_rsa.pub Datei?
<details><summary>:white_check_mark: Antwort</summary><p>

 Private und Public Key 	
</p></details>

---

:question: Wo darf ein SSH Tunnel nicht angewendet werden?
<details><summary>:white_check_mark: Antwort</summary><p>

 In der Firma	
</p></details>

---

:question: Für was dient die Datei `authorized_keys`?
<details><summary>:white_check_mark: Antwort</summary><p>

 Beinhaltet die Public Key von allen wo ohne Password auf System dürfen
</p></details>

---	

:question: Für was dient die Datei `known_hosts`?
<details><summary>:white_check_mark: Antwort</summary><p>

 Die Datei enthält eine Liste der Systeme die via ssh Verbunden wurden.

 Verifizierung des Servers: Die known_hosts Datei dient der Speicherung der öffentlichen SSH-Schlüssel der Hosts (Server), zu denen bereits eine SSH-Verbindung hergestellt wurde. Bei jedem Verbindungsversuch vergleicht der SSH-Client den in der known_hosts Datei gespeicherten Schlüssel mit dem vom Server präsentierten Schlüssel. Dies hilft zu verifizieren, dass der Client mit dem tatsächlichen Server und nicht mit einem Angreifer (im Rahmen eines Man-in-the-Middle-Angriffs) kommuniziert.
 
 Einträge in der known_hosts Datei können bei Veränderung eines bekannten Hosts angepasst werden, überflüssige oder nicht mehr benötigte Einträge müssen gelöscht werden. Änderungen können manuell mit einem Texteditor oder mit SSH-Tools wie ssh-keygen erfolgen.
</p></details>

---
